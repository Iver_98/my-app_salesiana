package com.example.demo_app

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView


class CustomAdapter: RecyclerView.Adapter<CustomAdapter.ViewHolder>(){
    /**ARRAYS QUE VAN A SER LISTADOS */
    private val images = intArrayOf(
        R.drawable.menu_ic2_desc_lp,
        R.drawable.menu_ic2_desc_lp,
        R.drawable.menu_ic2_desc_lp,
        R.drawable.menu_ic2_desc_lp,
        R.drawable.menu_ic2_desc_lp,
        R.drawable.menu_ic2_desc_lp,
    )

    private val titles = arrayOf("Codelia", "Suscribete", "Videos", "Facebook", "SodaStereo", "Spinneta")
    private val detail = arrayOf("Tutorial", "Canal", "descripcion", "habilitado", "Musico", "Musico")



    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ViewHolder {
        val v = LayoutInflater.from(viewGroup.context).inflate(R.layout.card_layout, viewGroup, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(v_holder:ViewHolder, position: Int) {
        v_holder.itemImage.setImageResource(images[position])
        v_holder.itemTitle.text = titles[position]
        v_holder.itemDetail.text = detail[position]
        }

    override fun getItemCount(): Int {
        return titles.size
    }


    inner class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        var itemImage: ImageView = itemView.findViewById(R.id.item_imagen)
        var itemTitle: TextView = itemView.findViewById(R.id.item_title)
        var itemDetail: TextView = itemView.findViewById(R.id.item_detail)

        init {
            itemView.setOnClickListener{ val position:Int = adapterPosition

                when(position+1){
                    1->{
                        val intent = Intent(itemView.context, DescLaPazActivity2::class.java)
                        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
                        itemView.context.startActivity(intent)
                    }
                    2->{
                        Toast.makeText(itemView.context,R.string.j, Toast.LENGTH_SHORT).show()
                    }
                    3->{
                        Toast.makeText(itemView.context,R.string.j, Toast.LENGTH_SHORT).show()
                    }
                    4->{
                        Toast.makeText(itemView.context,R.string.j, Toast.LENGTH_SHORT).show()
                    }
                    5->{
                        Toast.makeText(itemView.context,R.string.j, Toast.LENGTH_SHORT).show()
                    }
                    6->{
                        Toast.makeText(itemView.context,R.string.j, Toast.LENGTH_SHORT).show()
                    }
                }
            }
        }

    }

}
