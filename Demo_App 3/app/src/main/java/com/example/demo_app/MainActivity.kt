package com.example.demo_app

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

import android.view.MenuItem

import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.core.view.GravityCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.navigation.NavigationView
import kotlinx.android.synthetic.main.activity_main.*

import kotlinx.android.synthetic.main.toolbar.*


class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        /**TITULO DE ACTION BAR*/
        setSupportActionBar(toolbar)
        val actionBar = supportActionBar
        actionBar?.setTitle(R.string.principal)


        /**TOGGLE*/
        val drawertoggle : ActionBarDrawerToggle = object : ActionBarDrawerToggle(
            this, drawer_layout, toolbar, (R.string.open), (R.string.close)
        ) {
        }
        drawertoggle.isDrawerIndicatorEnabled = true
        drawer_layout.addDrawerListener(drawertoggle)
        drawertoggle.syncState()

        nav_view.setNavigationItemSelectedListener(this)
        nav_view.setCheckedItem(R.id.nav_principal)


        /**IMPLEMENTACION DEL RECYCLERVIEW*/
        val recyclerView = findViewById<RecyclerView>(R.id.recycler_view)
        val adapter = CustomAdapter()
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = adapter
    }/**/


    /**ENCARGADO DE LEER LOS ELEMENTOS DEL MENU**/
    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.nav_descubre -> {
                val intent = Intent(this, DescLaPazActivity2::class.java)
                startActivity(intent)
            }
            else -> false
        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)){
            drawer_layout.closeDrawer(GravityCompat.START)
        }
        else {
            super.onBackPressed()
        }
    }



}/**/