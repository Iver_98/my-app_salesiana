package com.example.demo_app

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.core.view.GravityCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView


import com.google.android.material.navigation.NavigationView
import kotlinx.android.synthetic.main.activity_desc_la_paz2.*

import kotlinx.android.synthetic.main.activity_main.drawer_layout
import kotlinx.android.synthetic.main.activity_main.nav_view
import kotlinx.android.synthetic.main.toolbar.*

class DescLaPazActivity2 : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_desc_la_paz2)

        /**TOOLBAR*/
        setSupportActionBar(toolbar)
        val actionBar = supportActionBar
        actionBar?.setTitle(R.string.Descubre_La_Paz)
        /**TOOLBAR*/

        /** RECIBIR DATOS-TOOLBAR DESDE UN INTENT PUTEXTRA
        setSupportActionBar(toolbar)
        val toolbarTitle = intent.getStringExtra("thisName")
        val actionBar = supportActionBar
        actionBar?.title = toolbarTitle */


        /**TOGGLE*/
        val drawertoggle : ActionBarDrawerToggle = object : ActionBarDrawerToggle(
            this, drawer_layout, toolbar, (R.string.open), (R.string.close)
        ) {

        }

        drawertoggle.isDrawerIndicatorEnabled = true
        drawer_layout.addDrawerListener(drawertoggle)
        drawertoggle.syncState()

        nav_view.setNavigationItemSelectedListener(this)
        nav_view.setCheckedItem(R.id.nav_descubre)
        /**TOGGLE*/


        /**RECYCLER VIEW*/
//        val recycler = findViewById<RecyclerView>(R.id.secondrecycler)
//        secondrecycler.layoutManager = LinearLayoutManager(this)
//        secondrecycler.adapter = CustomAdapter2()
        /**RECYCLER VIEW*/

    }

    /**NAVEGACION DEL DRAWER MENU*/
    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.nav_principal-> {
                val intent = Intent(this, MainActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
                startActivity(intent)
            }
            else ->false
        }
        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)){
            drawer_layout.closeDrawer(GravityCompat.START)
        }
        else{
            super.onBackPressed()
        }
    }
    /**NAVEGACION DEL DRAWER MENU*/

}