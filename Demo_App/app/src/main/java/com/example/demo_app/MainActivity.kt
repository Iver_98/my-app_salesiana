package com.example.demo_app

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.core.view.GravityCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.demo_app.adapters.CustomAdapter

import com.google.android.material.navigation.NavigationView
import kotlinx.android.synthetic.main.activity_main.*

import kotlinx.android.synthetic.main.toolbar.*


class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {
/*
    /***/

    private var titlesList = mutableListOf<String>()
    private var descList = mutableListOf<String>()
    private var imageslist = mutableListOf<Int>()

    /***/
*/

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)



        /***///postList()

        /**TITULO DE ACTION BAR*/
        setSupportActionBar(toolbar)
        val actionBar = supportActionBar
        actionBar?.setTitle(R.string.principal)


        /**TOGGLE*/
        val drawertoggle : ActionBarDrawerToggle = object : ActionBarDrawerToggle(
            this, drawer_layout, toolbar, (R.string.open), (R.string.close)
        ) {
        }
        drawertoggle.isDrawerIndicatorEnabled = true
        drawer_layout.addDrawerListener(drawertoggle)
        drawertoggle.syncState()

        nav_view.setNavigationItemSelectedListener(this)
        nav_view.setCheckedItem(R.id.nav_principal)

/*
        /**IMPLEMENTACION DEL RECYCLERVIEW*/
        val recyclerView = findViewById<RecyclerView>(R.id.recycler_view)
        val adapter = CustomAdapter()
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = adapter

        recyclerView.setBackgroundColor(Color.GRAY)
*/
        /**IMPLEMENTACION DEL RECYCLERVIEW*/
        recycler_view.layoutManager = LinearLayoutManager(this)
        recycler_view.adapter = CustomAdapter()
    }/**/


    /*
/**SEGUNDO INTENTO*/
    private fun addToList(title: String, description:String, image: Int){
        titlesList.add(title)
        descList.add(title)
        imageslist.add(image)
    }

    private fun postList(){
        for(i in 1..15){
            addToList("Title $i", "Description $i", R.mipmap.ic_launcher)
        }
    }*/



    /**ENCARGADO DE LEER LOS ELEMENTOS DEL MENU**/
    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.nav_descubre -> {
                val intent = Intent(this, DescLaPazActivity2::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
                startActivity(intent)
            }
            else -> false
        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)){
            drawer_layout.closeDrawer(GravityCompat.START)
        }
        else{
            super.onBackPressed()
        }
    }
/*
    private fun onListItemClick(position: Int) {
        Toast.makeText(this, mRepos[position].name, Toast.LENGTH_SHORT).show()
    }

*/

}/**/