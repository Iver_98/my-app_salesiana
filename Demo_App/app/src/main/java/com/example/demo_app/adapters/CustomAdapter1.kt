package com.example.demo_app.adapters

import android.content.Intent

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.example.demo_app.DescLaPazActivity2

/**Importacion dentro de otro paquete*/
import com.example.demo_app.R

/**ADAPTER - RECYCLER VIEW_CARDS*/
/**ADAPTER CORRESPONDIENTE A MAIN ACTIVITY*/
class CustomAdapter: RecyclerView.Adapter<CustomViewHolder>(){

    private val image = intArrayOf(
        R.drawable.nat31,
        R.drawable.nat42,
        R.drawable.nat48,
        R.drawable.nat55,
    )
    private val titles = intArrayOf(
        R.string.Descubre_La_Paz,
        R.string.atractivos,
        R.string.audioguias,
        R.string.ajustes,
    )

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
       /* val lay = LayoutInflater.from(parent.context)
        val cellRow = lay.inflate(R.layout.second_card, parent, false)
        return CustomViewHolder(cellRow)*/
        val card = LayoutInflater.from(parent.context).inflate(R.layout.card_layout, parent, false)
        return CustomViewHolder(card)
    }


    override fun onBindViewHolder(holder: CustomViewHolder, position: Int){
        holder.itemImage.setImageResource(image[position])
        holder.itemText.setText(titles[position])

    /*val img = image.get(position)
        holder.view.imageView.setImageResource(img)

        val titl = titles.get(position)
        holder.view.textView.setText(titl)*/
    }

    override fun getItemCount(): Int {
        return titles.size
    }

}


class CustomViewHolder (itemView:View):RecyclerView.ViewHolder(itemView){
    var itemImage: ImageView = itemView.findViewById(R.id.imageView)
    var itemText: TextView = itemView.findViewById(R.id.textView)

    init
    {
        itemView.setOnClickListener{val position = adapterPosition
            when(position+1){
                1 -> {
                    val intent = Intent(itemView.context, DescLaPazActivity2::class.java)
                    /**SIRVE PARA ENVIAR DATOS DE UN INTENT A OTRO
                    intent.putExtra("thisName", R.string.app_name)*/
                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
                    itemView.context.startActivity(intent)
                }
                2 -> {
                    Toast.makeText(itemView.context, R.string.j, Toast.LENGTH_SHORT).show()
                }
                3 -> {
                    Toast.makeText(itemView.context, R.string.j, Toast.LENGTH_SHORT).show()

                }
                4 -> {
                    Toast.makeText(itemView.context, R.string.j, Toast.LENGTH_SHORT).show()

                }
            }
        }
    }

}


