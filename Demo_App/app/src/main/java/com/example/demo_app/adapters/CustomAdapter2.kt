package com.example.demo_app.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView

/**Importacion dentro de otro paquete*/
import com.example.demo_app.R



/**ADAPTER - RECYCLER VIEW_CARDS*/
/**ADAPTER CORRESPONDIENTE A DESCUBRE LA PAZ*/

class CustomAdapter2 :RecyclerView.Adapter<CustomViewHolder2>(){

    private val image = intArrayOf(
        R.drawable.nat55,
        R.drawable.nat55,
        R.drawable.nat55,
        R.drawable.nat55,
    )
    private val titles = intArrayOf(
        R.string.j,
        R.string.j,
        R.string.j,
        R.string.j,
    )

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder2 {
        val cardD = LayoutInflater.from(parent.context).inflate(R.layout.second_card, parent, false)
        return CustomViewHolder2(cardD)
    }



    override fun onBindViewHolder(holder: CustomViewHolder2, position: Int) {
        holder.itemImage.setImageResource(image[position])
        holder.itemText.setText(titles[position])
    }


    override fun getItemCount(): Int {
        return titles.size
    }

}

class CustomViewHolder2 (itemView:View):RecyclerView.ViewHolder(itemView){
    var itemImage:ImageView = itemView.findViewById(R.id.imageView2)
    var itemText:TextView = itemView.findViewById(R.id.textView2)

    init
    {
        itemView.setOnClickListener { val position = adapterPosition
            when(position+1){
                1->
                {
                    Navigation.findNavController(itemView).navigate(R.id.gopantalla1)
                }
                2->
                {
                    Navigation.findNavController(itemView).navigate(R.id.gopantalla2)
                }
                3->
                {
                    Toast.makeText(itemView.context, R.string.Descubre_La_Paz, Toast.LENGTH_SHORT).show()
                }
                4->
                {
                    Toast.makeText(itemView.context, R.string.Descubre_La_Paz, Toast.LENGTH_SHORT).show()
                }

            }

        }
    }

}