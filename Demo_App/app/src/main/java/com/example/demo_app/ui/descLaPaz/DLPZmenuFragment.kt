package com.example.demo_app.ui.descLaPaz

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import androidx.recyclerview.widget.LinearLayoutManager
import com.example.demo_app.R
import com.example.demo_app.adapters.CustomAdapter2

import kotlinx.android.synthetic.main.fragment_d_l_p_zmenu.*

class DLPZmenuFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        val view:View = inflater.inflate(R.layout.fragment_d_l_p_zmenu, container, false)

//        view.bt1.setOnClickListener { Navigation.findNavController(view).navigate(R.id.gopantalla1) }
//        view.bt2.setOnClickListener { Navigation.findNavController(view).navigate(R.id.gopantalla2) }
//
//        view.bt3.setOnClickListener {
//            Toast.makeText(view.context, R.string.j, Toast.LENGTH_SHORT).show()
//        }
//        view.bt4.setOnClickListener {
//            Toast.makeText(view.context, R.string.j, Toast.LENGTH_SHORT).show()
//        }
//        recyclerDLPZ_MENU.layoutManager = LinearLayoutManager(activity)
//        recyclerDLPZ_MENU.adapter = CustomAdapter2()

        return view
    }
    /**MOSTRAR RECYCLERVIEW EN EL FRAGMENT*/
    override fun onViewCreated(itemv: View, savedInstanceState: Bundle?) {
        super.onViewCreated(itemv, savedInstanceState)
        recyclerDLPZ_MENU.apply {
            layoutManager = LinearLayoutManager(activity)
            adapter = CustomAdapter2()
        }
    }

}