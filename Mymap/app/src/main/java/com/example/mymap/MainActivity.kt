package com.example.mymap

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.google.android.gms.maps.CameraUpdateFactory

import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions

class MainActivity : AppCompatActivity(), OnMapReadyCallback{

    private lateinit var map: GoogleMap

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        createFragment()
    }

    private fun createFragment() {
        val mapFragment = supportFragmentManager.findFragmentById(R.id.mapFragment) as SupportMapFragment
        mapFragment.getMapAsync(this)

    }

    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap
        creteMarker()
    }

    private fun creteMarker() {
        val murillo = LatLng(-16.4922085, -68.1357346)

        val plaza = map.addMarker(MarkerOptions().position(murillo).title(" RUE JAÉN "))
        map.animateCamera(CameraUpdateFactory.newLatLngZoom(murillo, 18f), 2000, null)
        plaza?.showInfoWindow()

//        -16.4922085!4d-68.1357346


//        map.addMarker(MarkerOptions().position(place).title("plaza"))
//        map.animateCamera(
//            CameraUpdateFactory.newLatLngZoom(place, 15f), 2000, null)
//        val marker = MarkerOptions().position(place).title("Esto es el Museo")
//        map.addMarker(marker)

//        marker.visible(true)

    }
}
